<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/08/18
 * Time: 19:51
 */

namespace App\Service;


class BarERP implements ERP
{

    private $url;
    private $requestService;

    public function __construct()
    {
        $this->url = 'https://api.dev.redevistorias.com.br/challenge/service/bar/building/';
        $this->requestService = $this->requestService = new RequestService();
    }

    public function getPropertyValue($cod)
    {
        $fullURL = $this->url.$cod;
        $data = $this->requestService->getData($fullURL);
        $input = $this->generateInputToApi($data);

        $price = $this->requestService->sendDataToApi($input);

        return $price;
    }

    private function generateInputToApi($data)
    {
        $infos = [];

        $infos['building_type'] = $data['building']['type'];
        $infos['inspection_type'] = $data['inspection']['type'];
        $infos['area'] = $data['building']['area'];
        $infos['furnished'] = $data['building']['furnished'];
        $infos['modality'] = $data['inspection']['modality'];

        $infos['express'] = true;

        if(is_null($data['inspection']['express']) || strlen($data['inspection']['express']) == 0) {
            $infos['express'] = false;
        }

        return $infos;
    }

}