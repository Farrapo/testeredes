<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/08/18
 * Time: 20:07
 */

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

class RequestService
{
    private $guzzleClient;

    public function __construct()
    {
        $this->guzzleClient = new Client();
    }

    public function getData($url) {
        try {
            $res = $this->guzzleClient->request('GET', $url);

            if( $res->getStatusCode() != 200 ) {
                throw new ServiceUnavailableHttpException(null, "API not available");
            }

            $data = json_decode($res->getBody(), true);

            return $data;
        } catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
            throw new ServiceUnavailableHttpException(null, "API not available");
        } catch (\Exception $e) {
            throw new ServiceUnavailableHttpException(null, "API not available");
        }
    }

    public function sendDataToApi($data)
    {
        $url = 'https://api.dev.redevistorias.com.br/erp/price_preview';

        $headers = [
            'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImYzNzFhZTVkZjFhYjQxM2ZkYTkxM2JkM2E1NGY0MDIzNDM2NTliNmMwOTAxNDVjMDgyNTdlMWNiNjFlNmJkMjg0ZGMzNWVhNTJkOTRlMDVhIn0.eyJhdWQiOiJyZWRldmlzdG9yaWFzIiwianRpIjoiZjM3MWFlNWRmMWFiNDEzZmRhOTEzYmQzYTU0ZjQwMjM0MzY1OWI2YzA5MDE0NWMwODI1N2UxY2I2MWU2YmQyODRkYzM1ZWE1MmQ5NGUwNWEiLCJpYXQiOjE1MzQzNDA0MDgsIm5iZiI6MTUzNDM0MDQwOCwiZXhwIjoxNTM1MDMxNjA4LCJzdWIiOiJydmRldkByZWRldmlzdG9yaWFzLmNvbS5iciIsInNjb3BlcyI6WyJvcmRlcjpjcmVhdGUiLCJvcmRlcjp2aWV3Iiwib3JkZXI6ZWRpdCIsImNsaWVudDpjcmVhdGUiLCJjbGllbnQ6ZGVsZXRlIiwiY2xpZW50OmVkaXQiLCJjbGllbnQ6dmlldyJdfQ.jg55yaCQV2xAo9E-YYHJ17OPqJ2MwJ9YZ-DI_um3w_iI8NlCQKjqT5p4_krIXhpjWxMzwY4ikkE3GYxUFuahKw-9IACfCtbnlB1OW5OhZ8RBqiABKb6prZKNqOe74stILSKW_mT40BAbYihxZGA2-_CX-VByiTUcbi8dQ4vGMVaqj2aOVnQEtktijDWw0ANF1toU4xp3pj8ecTVv6BfNsQtWb06o1y-_b5EwxVOaHDojxfnsZzzAnRMPFWwXLPMekPaRsokzIpAZZxbceCxABnv-XfJEvZ_Low9_pBQlcQSp4CKSLLxp6Sr4au-ff__hW_tQdZp5T6dAKay92CqkGA',
            'Content-Type'        => 'application/json',
        ];

        try {
            $response = $this->guzzleClient->request('POST', $url, ['json' => $data, 'headers' => $headers]);

            $price = json_decode($response->getBody()->getContents());

            return $price;
        } catch (GuzzleException $e) {
            var_dump('Erro no guzzle');
            var_dump($e->getMessage());
            die(0);
        }
    }
}