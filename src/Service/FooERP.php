<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/08/18
 * Time: 19:39
 */

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class FooERP implements ERP
{
    private $url;
    private $requestService;

    public function __construct()
    {
        $this->url = 'https://api.dev.redevistorias.com.br/challenge/service/foo/item/';
        $this->requestService = new RequestService();
    }

    public function getPropertyValue($cod)
    {
        $fullURL = $this->url.$cod;
        $data = $this->requestService->getData($fullURL);
        $input = $this->generateInputToApi($data);

        $price = $this->requestService->sendDataToApi($input);

        return $price;
    }

    private function generateInputToApi($data) {
        $infos = [];
        $infos['building_type'] = $data['edifice_type'];
        $infos['inspection_type'] = $data['inspection_type'];
        $infos['area'] = $data['area'];
        $infos['furnished'] = $data['furnished'];
        $infos['modality'] = $data['inspection_modality'];
        $infos['express'] = $data['inspection_express'];

        return $infos;
    }
}