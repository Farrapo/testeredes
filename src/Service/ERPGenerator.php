<?php

namespace App\Service;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ERPGenerator
{
    public function generate($erp) {
        if($erp === 'foo') {
            return new FooERP();
        } else if ($erp === 'bar') {
            return new BarERP();
        } else {
            throw new NotFoundHttpException('ERP informado não pôde ser encontrado');
        }
    }
}