<?php

namespace App\Controller;

use App\Service\ERPGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ServiceController extends AbstractController
{
    private $erpService;
    /**
     * @Route("/price/{erp}/{cod}", name="check_price")
     */
    public function check_price($erp, $cod, ERPGenerator $ERPGenerator) {

        $this->erpService = $ERPGenerator->generate($erp);

        $price = $this->erpService->getPropertyValue($cod);

        return $this->json($price);
    }

}