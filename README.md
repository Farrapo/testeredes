# Projeto Back-End como parte do processo seletivo da Redes Vistorias

Este projeto foi desenvolvido utilizando o framework Symfony 4,
e para fazer as requisições HTTP, foi utilizada a biblioteca Guzzle.


## Como instalar

Para instalar as dependências, deve-se utilizar o 
[Composer](https://getcomposer.org/), e executar o comando:

composer install


## Estrutura do projeto

Optei por criar uma interface (ERP) contendo apenas um método.
Futuramente essa interface poderia ter mais funções e constantes.

A seguir, criei duas classes (FooERP e BarERP) que implementam
essa interface. Assim, a criação de novos tipos de ERP é bastante
simplificada, bastando implementar a interface ERP.

Há também um ERPGenerator, que é o responsável pela inicialização
correta do ERP, conforme argumento recebido na função. Caso esse
argumento não seja encontrado, um erro é lançado.

Por fim, o controller, através da injeção de dependências, só
precisa ter acesso ao ERPGenerator, que retorna a instância correta.


A última classe dentro do pacote Service é o RequestService. Eu criei
essa classe para centralizar as requisições (GET e POST), utilizando
a biblioteca Guzzle.


## TODO List e aspectos a melhorar

Foi a primeira vez que utilizei o Symfony como framework (estou mais
familiarizado com o Laravel, que utiliza o Symfony por baixo
dos panos), assim, há alguns aspectos que acredito que possam
ser melhorados.

Por exemplo, armazenar as informações de tokens em um arquivo (.env)
e carregar automaticamente no código. Outro exemplo são os testes unitários e de
integração, que só fiz um, para entender como o Symfony trabalha.

Outros testes seriam necessários, para garantir que as requisições retornam
o esperado.